from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="index"),
    path('uploadfile/', FileUploadView.as_view()),
    path('uploadfile/<int:pk>/', FileDownloadView.as_view()),
]
