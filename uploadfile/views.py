from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.parsers import FileUploadParser

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .serializers import FileSerializer
from .models import File
from django.http import HttpResponse
from rest_framework.permissions import IsAuthenticated
from .tasks import count_words

def index(request):
    return HttpResponse("Usage:<br><br> \
                        Syntax:  curl -s <url> -F fieldname=@&lt;filetoupload&gt; <br>\
                        Example: curl -s http://127.0.0.1:8000/uploadfile/ -F file=@test.txt"\
                        )


class FileDownloadView(APIView):
    def get(self, request, pk):
        file = File.objects.get(pk=pk)
        file_data = file.file.read()
        return Response({"File ID: ": file.id, "File data: " : file_data},
                    status=status.HTTP_200_OK)


class FileUploadView(APIView):
    parser_class = (MultiPartParser, FormParser)
    permission_classes = [IsAuthenticated]

    # Sample CURL request
    # curl -s http://127.0.0.1:8000/uploadfile/ -F 'file=@myfile.txt;filename=someotherfilename.txt'

    def post(self, request, *args, **kwargs):
        data = request.data

        # Get the file object
        file = data['file']
        # Read file
        file_data = file.read()
        # Split into words and store in a list
        words_in_file = file_data.split()
        # Count no of words and store it
        data['words'] = len(words_in_file)

        # Get the file-name a) Name of file uploaded or b) The explicit file name specified in curl request
        data['filename'] = file.name

        file_serializer = FileSerializer(data=data)

        if file_serializer.is_valid():
            file_obj = file_serializer.save()
            count_words.delay(file_obj.id)
            return Response({"status": "Success", "headers": request.headers, "data" : data, "File ID: ": file_obj.id},status=status.HTTP_201_CREATED)
        else:
            return Response({"status": "Failure", "headers": request.headers, "data": request.data},status=status.HTTP_400_BAD_REQUEST)
