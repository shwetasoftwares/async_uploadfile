from rest_framework import serializers
from uploadfile.models import *


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('file','filename','words',)

