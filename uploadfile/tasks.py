from celery import shared_task
from .models import File


@shared_task
def count_words(file_id):
    file = File.objects.get(pk=file_id)
    file_data = file.file.read()

    # Split into words and store in a list
    words_in_file = file_data.split()
    # Count no of words and store it
    total = len(words_in_file)

    return 'Number of words: {}'.format(total)
