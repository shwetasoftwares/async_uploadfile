from django.db import models


class File(models.Model):
    file = models.FileField()
    filename = models.CharField(max_length=100)
    words = models.IntegerField(default=0)

    def __str__(self):
        return self.file.name

